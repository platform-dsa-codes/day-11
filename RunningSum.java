import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public int[] runningSum(int[] nums) {
        int n = nums.length;
        
        for (int i = 1; i < n; i++) {
            nums[i] += nums[i - 1];
        }
        
        return nums;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of elements in the array: ");
        int n = scanner.nextInt();
        
        int[] nums = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        Solution solution = new Solution();

        int[] result = solution.runningSum(nums);

        System.out.println("Running sum array: " + Arrays.toString(result));

        scanner.close();
    }
}
