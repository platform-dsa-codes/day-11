import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public int[][] construct2DArray(int[] original, int m, int n) {
        int totalElements = m * n;

        if (original.length != totalElements) {
            return new int[0][0];
        }

        int[][] result = new int[m][n];
        int index = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                result[i][j] = original[index++];
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int len = scanner.nextInt();
        int[] original = new int[len];
        System.out.println("Enter the elements of the array (original):");
        for (int i = 0; i < len; i++) {
            original[i] = scanner.nextInt();
        }

        System.out.print("Enter the number of rows (m): ");
        int m = scanner.nextInt();

        System.out.print("Enter the number of columns (n): ");
        int n = scanner.nextInt();

        Solution solution = new Solution();

        int[][] result = solution.construct2DArray(original, m, n);

        System.out.println("Constructed 2D array: " + Arrays.deepToString(result));

        scanner.close();
    }
}
